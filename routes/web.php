<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Login
$router->post('/login', ['uses' => 'Users\AuthController@signin']);

// Register
$router->put('/user', ['uses' => 'Users\AuthController@signup']);

$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
    // Send a new message
    $router->put('/message', ['uses' => 'Messages\MessageController@send']);

    // Read a chat history
    $router->get('/message/{id}', ['uses' => 'Messages\MessageController@getHistory']);
});