<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 25/11/2017
 * Time: 15:57
 */

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
//use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'name', 'password', 'username'
    ];

    private $inbox = [];

    /**
     * @return array
     */
    public function getInbox(): array
    {
        return $this->inbox;
    }

    /**
     * @param array $inbox
     * @return User
     */
    public function setInbox(array $inbox): User
    {
        $this->inbox = $inbox;
        return $this;
    }


}