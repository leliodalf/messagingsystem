<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 25/11/2017
 * Time: 15:33
 */

namespace  App\Models\Messages;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'body', 'user_from', 'user_to'
    ];


    private $id;
    private $body;
    private $user_from;
    private $user_to;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return Message
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return integer
     */
    public function getUserFrom()
    {
        return $this->user_from;
    }

    /**
     * @param integer $user_from
     */
    public function setUserFrom($user_from)
    {
        $this->user_from = $user_from;
    }

    /**
     * @return integer
     */
    public function getUserTo()
    {
        return $this->user_to;
    }

    /**
     * @param mixed $user_to
     */
    public function setUserTo($user_to)
    {
        $this->user_to = $user_to;
    }





}