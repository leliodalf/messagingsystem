<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 26/11/2017
 * Time: 11:13
 */

namespace App\Http\Controllers\Messages;

use App\Manager\Messages\MessageManager;
use App\Models\Users\User;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MessageController extends BaseController
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Function for to save a new message for a specific user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function send(){
        try{

            $this->validate($this->request,[
                "body"=>"required",
                "to" => "required|integer"
            ]);

            $to = (int)$this->request->json("to");
            $body = $this->request->json("body");
            $user = $this->request->auth;

            $userTo = User::find($to);
            if(!$userTo){
                throw new EntityNotFoundException("User to", $to);
            }
            $messageManager = new MessageManager();
            $messageManager->save($user->id, $to, $body);
            return response()->json(["message" => "Message saved"],Response::HTTP_CREATED);
        }
        catch (EntityNotFoundException $entityNotFoundException){
            return response()->json($entityNotFoundException->getMessage(),Response::HTTP_BAD_REQUEST);
        }


    }

    /**
     * Function for to get the history chat with a specific user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistory($id){
        try{
            $user = $this->request->auth;
            $from = $user->id;
            if($id == $from){
                throw new BadRequestHttpException("Request id can't be the same of the user logged");
            }

            $userTo = User::find($id);
            if(!$userTo){
                throw new EntityNotFoundException("User to", $id);
            }

            $messageManager = new MessageManager();
            $list = $messageManager->getHistory($id,$from);
            return response()->json($list,Response::HTTP_OK);
        }
        catch (BadRequestHttpException $exception){
            return response()->json($exception->getMessage(),Response::HTTP_BAD_REQUEST);
        }
        catch (EntityNotFoundException $entityNotFoundException){
            return response()->json($entityNotFoundException->getMessage(),Response::HTTP_BAD_REQUEST);
        }


    }

}