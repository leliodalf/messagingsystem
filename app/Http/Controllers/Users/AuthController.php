<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 26/11/2017
 * Time: 10:51
 */

namespace App\Http\Controllers\Users;

use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use App\Models\Users\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     *
     * @param  \App\Models\Users\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "chat-jwt",
            'sub' => $user->id,
            'usr' => $user->username,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\Models\Users\User $user
     * @return mixed
     */
    public function signin(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        $user = User::where('email', $this->request->json('email'))->first();

        if (!$user) {
            return response()->json([
                'error' => 'Email does not exist.'
            ], Response::HTTP_BAD_REQUEST);
        }

        if (Hash::check($this->request->json('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user)
            ], Response::HTTP_OK);
        }

        return response()->json([
            'error' => 'Email or password is wrong.'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Function for register a new user
     *
     * @return \Illuminate\Http\JsonResponse|null|Response
     * @throws \Exception
     */
    public function signup(){

        try{
            $this->validate($this->request,[
                "name" => "required|max:50",
                "username" => "required|max:50",
                "email" => "required|email|max:250",
                "password" => "required|max:30",
            ]);

            $user = new User();
            $user->name = $this->request->json("name");
            $user->username = $this->request->json("username");
            $user->password = Hash::make($this->request->json("password"));
            $user->email = $this->request->json("email");

            $res = $user->save();
            if(!$res){
                throw new \Exception("Error saving data");
            }
            return response()->json(["message" => "User created"],Response::HTTP_CREATED);
        }
        catch (ValidationException $validationException){
            return $validationException->getResponse();
        }
        catch (QueryException $exception){
            return response()->json(["error"=>"User already exists"],Response::HTTP_CONFLICT);
        }

    }
}