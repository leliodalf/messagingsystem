<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 26/11/2017
 * Time: 10:53
 */

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\Users\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;

class JwtMiddleware
{
    public function handle(Request $request, Closure $next, $guard = null)
    {

        $token = $request->header("Authentication");
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }

        $user = new User();
        $user->id = $credentials->sub;
        $user->username = $credentials->usr;

        $request->auth = $user;
        return $next($request);
    }
}