<?php
/**
 * Created by PhpStorm.
 * User: lelio
 * Date: 26/11/2017
 * Time: 12:00
 */

namespace  App\Manager\Messages;

use App\Models\Messages\Message;

final class MessageManager
{
    /**
     * Function for to save a new message
     * @param string $userFrom User id of the sender
     * @param string $userTo User id of the receiver
     * @param string $body Body of the message
     * @return Message
     * @throws \Exception
     */
    public function save($userFrom, $userTo, $body){
        $message = new Message([
            "body" => $body,
            "user_to" => $userTo,
            "user_from" => $userFrom,

        ]);
        $res = $message->save();
        if(!$res){
            throw new \Exception("Error saving data");
        }
        return $message;
    }

    /**
     * Return a list of messages between two users
     * @param $userTo
     * @param $userFrom
     * @return Message[]
     */
    public function getHistory($userTo, $userFrom){
        return Message::where(function($query) use ($userTo, $userFrom){
            $query->where('user_to', '=', $userTo)
                ->where('user_from', '=', $userFrom);
        })->orWhere(function ($query) use ($userTo, $userFrom) {
            $query->where('user_from', '=', $userTo)
                ->where('user_to', '=', $userFrom);
        })->get();
    }
}