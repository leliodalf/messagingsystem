<?php
require_once __DIR__.'/../bootstrap/app.php';
class CallsTest extends TestCase
{
    const user_name = "test";
    const user_email = "email2@email.com";
    const user_username = "username3";
    const user_password = "test123";

    const user_user_to = 11;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }
    

    /**
     * Register an user test.
     *
     * @return void
     */
    public function testRegisterUser()
    {
        $this->json('PUT','/user',[
            "name" => self::user_name,
	        "username" => self::user_username,
	        "email" => self::user_email,
	        "password" => self::user_password
        ])->seeJsonEquals(["message" => "User created"]);
    }

    /**
     * Login an user test.
     *
     * @return void
     */
    public function testLoginUser()
    {
        $res = $this->json('POST','/login',[
            "email" => self::user_email,
            "password" => self::user_password
        ])->seeJsonStructure(["token"]);
    }

    /**
     * Send a new message test.
     *
     * @return void
     */
    public function testSendMessage()
    {
        $res = $this->json('POST','/login',[
            "email" => self::user_email,
            "password" => self::user_password
        ]);
        $token = json_decode($res->response->getContent())->token;


        $res = $this->json('PUT','/message',[
            "to" => self::user_user_to,
            "body" => "test message"
        ],[
            'Authentication'=>$token
        ])->seeJsonEquals(["message" => "Message saved"]);
    }

    /**
     * Get a list of a messages test.
     *
     * @return void
     */
    public function testHistoryMessages()
    {
        $res = $this->json('POST','/login',[
            "email" => self::user_email,
            "password" => self::user_password
        ]);
        $token = json_decode($res->response->getContent())->token;

        $res = $this->json('GET','/message/'.self::user_user_to,[],[
            'Authentication'=>$token
        ]);

        $this->assertEquals(200, $res->response->status());
    }

}
