## About the project

In the project MessagingSystem you can save a new user, make a login, send a message to another user and read the message 
with between 2 users.

The system use SQLITE, with 2 tables (users, messages). In those tables are defined unique keys and foreign keys.

The project is based on Lumen (micro-framework of Laravel)

The user authentication is made with JWT-auth, inside a specific middleware.

When the user make the login, receive back a token, and this token must be send in very request as a value in the header named
Authentication.
Example.: Authentication:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjaGF0LWp3dCIsInN1YiI6MTgsInVzciI6InVzZXJuYW1lMiIsImlhdCI6MTUxMTcwODY0MCwiZXhwIjoxNTExNzEyMjQwfQ.XqEcz75n46I75YFJHABNNKaJoyMZZ3-wsJeShwdxcpo"

The end-point "Message" require this authentication

The end-points:

PUT /user {
          	"name" : "name",
          	"username" : "username",
          	"email" : "email@email.com",
          	"password" : "password"
          }
          
POST /login {
            	"email" : "eemail@email.com",
            	"password" : "password"
            }
            
(Require authentication) PUT /message {
             	"to": id of the user receiver ,
             	"body":"body of the message"
             }
            
(Require authentication) GET /message/{id of the user receiver}

The database file is located in database/sqlite.db

The file MessageSystem.postman_collection.json has an export the requests to import in PostMan